# Prueba Practica Frontend

## Dirigido a

Desarrolladores **Frontend** y **móviles** interesados en una posición de desarrollador.

## Objetivos

Implementar un cliente móvil que permita la navegación y operaciones a través de un árbol de nodos, proveniente de un
API REST.

- Implementar un cliente HTTP para el consumo del api.
- Implementar una interfaz de navegación sencilla.

## Requerimientos Funcionales

- El árbol está almacenado en una Base de Datos y es accedido mediante una API REST ubicada en la siguiente URL:
  https://api-graph.tests.grupoapok.com

- La definición del API y ejemplos de llamada se encuentra en una colección de [Insomnia](https://insomnia.rest/) que
  puede importar desde el archivo [api-docs/graph-api.json](https://gitlab.com/grupoapok/dev-position-tests/frontend/graph-api/-/blob/main/api-docs/graph-api.json)

- Los nodos poseen la siguiente estructura: `{"id": 1,"parent": null,"title": "one", }` Donde el campo parent contiene
  el ID del padre y si es null es un nodo raíz.

- Al inicio se deben mostrar todos los nodos padres y a partir de ahí ir navegando hacia los hijos haciendo click.

- Adicionalmente los nodos pueden traer traducciones para el campo title, desde el cliente web se debe poder cambiar el
  idioma en el que se ven los nodos.

- Se debe poder crear un nodo.

- Se deben poder eliminar nodos, siempre y cuando no tengan hijos.

## Requerimientos Técnicos

- El cliente debe estar diseñado e implementado basado las mejoras prácticas.
- Se deben gestionar apropiadamente los errores.
- No se evaluará diseño pero si facilidad de uso. 
- El Framework a usar será de su elección en función a la posición que aplican:
  - Frontend: Angular, React o Vue.
  - Mobile: Flutter, Kotlin o Swift.
- Pueden usar cualquier librería que consideren válida.
- Debe llevarse el proyecto con Git para control de versiones.
- La solución debe ser responsive

## Modo de Entrega

- Compilado para despliegue vía email a [reclutamiento+test@grupoapok.com](mailto:reclutamiento+test@grupoapok.com)
  - Zip con el dist para Web
  - apk o ipa instalable para mobile
- Enlace al repositorio público con los códigos fuentes de la aplicación

## Aspectos a Evaluar

- Estructura y estilo de código.
- Lógica de la solución.
- Cumplimiento de los requerimientos funcionales.


