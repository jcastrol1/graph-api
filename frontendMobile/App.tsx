import React from 'react';
import 'react-native-gesture-handler';
import { AppNavigation } from './src/routes/AppNavigation';

function App(): JSX.Element {
  return <AppNavigation />;
}

export default App;
