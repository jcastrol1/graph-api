import { useRef, useState, useEffect } from 'react';

export enum typeStateEnum {
  IDLE = 'idle',
  PENDING = 'pending',
  DONE = 'done',
  FAIL = 'fail',
}
export interface Idle {
  type: typeStateEnum.IDLE;
}

export interface Pending {
  type: typeStateEnum.PENDING;
}

export interface Done<T> {
  type: typeStateEnum.DONE;
  data: T;
}

export interface Fail {
  type: typeStateEnum.FAIL;
  error: unknown;
}

export type State<T> = Idle | Pending | Done<T> | Fail;
export type Signal = AbortController['signal'];
export type PromiseFn<R> = (signal: Signal) => Promise<R>;

export const useFetch = <T>() => {
  const ctrl = useRef<AbortController | null>(null);
  const [state, setState] = useState<State<T>>({ type: typeStateEnum.IDLE });

  const abort = () => {
    ctrl.current?.abort();
  };

  const handleFetch = async (promiseFn: PromiseFn<T>) => {
    abort();

    ctrl.current = new AbortController();

    setState({ type: typeStateEnum.PENDING });

    try {
      const data = await promiseFn(ctrl.current.signal);
      setState({ type: typeStateEnum.DONE, data });
    } catch (error: unknown) {
      if (ctrl.current.signal.aborted) {
        console.warn('Request aborted');
        return;
      }

      setState({ type: typeStateEnum.FAIL, error });
    }
  };

  useEffect(() => {
    return () => {
      abort();
    };
  }, []);

  return [state, handleFetch, abort] as const;
};
