export interface Node {
  id: number;
  parent: number;
  title: string;
  created_at: string | null;
  updated_at: string | null;
  translation?: Node[];
}
