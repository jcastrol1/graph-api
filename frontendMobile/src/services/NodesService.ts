import { Signal } from '../utils/hooks/useFetch';

export const NodesService = {
  getParentNodes: async (signal: Signal) => {
    try {
      const response = await fetch(
        'https://api-graph.tests.grupoapok.com/api/nodes',
        {
          signal,
        },
      );

      if (response.status < 200 || response.status >= 400) {
        return Promise.reject(new Error('Error'));
      }

      const nodes = await response.json();
      return nodes;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
  getChildNodes: async (id: number, signal: Signal) => {
    try {
      const response = await fetch(
        `https://api-graph.tests.grupoapok.com/api/nodes?parent=${id}`,
        {
          signal,
        },
      );
      if (response.status === 404) {
        return [];
      }
      if (response.status < 200 || response.status >= 400) {
        return Promise.reject(new Error('Error'));
      }

      const nodes = await response.json();
      return nodes;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
  saveNode: async (
    signal: Signal,
    data: { parent: number; locales: string[] },
  ) => {
    try {
      const response = await fetch(
        'https://api-graph.tests.grupoapok.com/api/node',
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
          signal,
        },
      );

      const node = await response.json();
      return node;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
  deleteNode: async (signal: Signal, id: number) => {
    try {
      const response = await fetch(
        `https://api-graph.tests.grupoapok.com/api/node/${id}`,
        {
          method: 'DELETE',
          signal,
        },
      );

      const node = await response.json();
      return node;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
  getNode: async (signal: Signal, id: number) => {
    try {
      const response = await fetch(
        `https://api-graph.tests.grupoapok.com/api/node/${id}`,
        {
          method: 'GET',
          signal,
        },
      );
      if (response.status < 200 || response.status >= 400) {
        return Promise.reject(new Error('Error'));
      }

      const node = await response.json();
      return node;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
  getNodewithLocale: async (signal: Signal, id: number, locale: string) => {
    try {
      const response = await fetch(
        `https://api-graph.tests.grupoapok.com/api/node/${id}?locale=${locale}`,
        {
          method: 'GET',
          signal,
        },
      );
      console.log(response.status);
      if (response.status < 200 || response.status >= 400) {
        return Promise.reject(new Error('Error'));
      }

      const node = await response.json();
      return node;
    } catch (error) {
      return Promise.reject(new Error('Error'));
    }
  },
};
