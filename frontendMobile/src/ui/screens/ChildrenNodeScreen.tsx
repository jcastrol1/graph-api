import React, { FC, useCallback, useState } from 'react';
import { SafeAreaView, StatusBar, View } from 'react-native';
import { StackHeaderProps } from '@react-navigation/stack/lib/typescript/src/types';
import { ListNode, ModalNote, SectionCreate } from '../components/nodes';
import { SectionDelete } from '../components/nodes/SectionDelete';
import { styles } from '../components/nodes/nodestyles';
import { ErrorBoundary } from '../containers/ErrorBoundary';
import { ErrorNode } from '../components/nodes/ErrorNode';
import Loading from '../components/common/Loading';
import { typeStateEnum } from '../../utils/hooks/useFetch';
import { useNodesFetch } from '../hooks/useNodesFetch';
import { useNodeFetch } from '../hooks/useNodeFetch';

type Action = 'view' | 'navigate';

type PayloadNavigate = { nodeId: number };
type PayloadView = { nodeId: number; locale: string };

type Payload = PayloadNavigate | PayloadView;
export type ActionHandler = ({
  action,
  payload,
}: {
  action: Action;
  payload: Payload;
}) => void;

const ChildrenNodeScreen: FC<StackHeaderProps> = (props: StackHeaderProps) => {
  const { route } = props;
  const { id } = route.params || { id: 0 };
  const [nodesState, refresh] = useNodesFetch(id);
  const [
    nodeState,
    handleGetFetchNode,
    handleCreateFetchNode,
    handleDeleteFetchNode,
  ] = useNodeFetch();

  const [visibleModal, setvisibleModal] = useState(false);
  const handleCloseModal = () => {
    setvisibleModal(false);
  };
  const validateIsEmpty =
    nodesState.type === typeStateEnum.DONE && nodesState.data.length === 0;

  const handleNavigation = useCallback((nodeId: number) => {
    props.navigation.push('Nodes', { id: nodeId });
  }, []);

  React.useLayoutEffect(() => {
    props.navigation.setOptions({
      title: `Child Nodes of ${id} `,
    });
  }, [props.navigation]);

  const handleGetNode = async (nodeId: number) => {
    setvisibleModal(true);
    await handleGetFetchNode({ id: nodeId });
  };
  const handleCreateNode = async () => {
    await handleCreateFetchNode(id, []);
    await refresh();
  };
  const handleDeleteNode = async () => {
    await handleDeleteFetchNode(id);
    await props.navigation.navigate('Home');
  };

  const onActions: ActionHandler = ({ action, payload }) => {
    switch (action) {
      case 'navigate':
        handleNavigation(payload.nodeId);
        break;
      case 'view':
        handleGetNode(payload.nodeId);
        break;
      default:
        console.error('Acción no válida');
    }
  };

  return (
    <SafeAreaView style={styles.flex1}>
      <StatusBar barStyle={'light-content'} />
      <ErrorBoundary fallBackComponent={<ErrorNode />}>
        <View style={styles.container}>
          <SectionCreate id={id} onPress={handleCreateNode} />
          {nodesState.type === typeStateEnum.PENDING && <Loading />}
          {nodesState.type === typeStateEnum.FAIL && <ErrorNode />}
          {validateIsEmpty && <SectionDelete onPress={handleDeleteNode} />}
          {nodesState.type === typeStateEnum.DONE && (
            <ListNode nodes={nodesState.data} onPress={onActions} />
          )}
        </View>

        <ModalNote
          node={nodeState}
          onPress={handleGetFetchNode}
          visible={visibleModal}
          onClose={handleCloseModal}
        />
      </ErrorBoundary>
    </SafeAreaView>
  );
};

export default ChildrenNodeScreen;
