import React, { useCallback, useState } from 'react';
import { SafeAreaView, StatusBar, View } from 'react-native';
import { StackHeaderProps } from '@react-navigation/stack/lib/typescript/src/types';
import { ListNode, ModalNote } from '../components/nodes';
import { styles } from '../components/nodes/nodestyles';
import { ErrorNode } from '../components/nodes/ErrorNode';
import { ErrorBoundary } from '../containers/ErrorBoundary';
import Loading from '../components/common/Loading';
import { typeStateEnum } from '../../utils/hooks/useFetch';
import { useNodesFetch } from '../hooks/useNodesFetch';
import { ActionHandler } from './ChildrenNodeScreen';
import { useNodeFetch } from '../hooks/useNodeFetch';

const ParentsNodeScreen = (props: StackHeaderProps) => {
  const [nodesState] = useNodesFetch(null);
  const [nodeState, handleGetFetchNode] = useNodeFetch();
  const [visibleModal, setvisibleModal] = useState(false);
  React.useLayoutEffect(() => {
    props.navigation.setOptions({
      title: 'Parent Nodes',
    });
  }, [props.navigation]);

  const handleNavigation = useCallback((nodeId: number) => {
    props.navigation.navigate('Nodes', { id: nodeId });
  }, []);
  const handleCloseModal = () => {
    setvisibleModal(false);
  };
  const handleGetNode = async (nodeId: number) => {
    setvisibleModal(true);
    await handleGetFetchNode({ id: nodeId });
  };
  const onActions: ActionHandler = ({ action, payload }) => {
    switch (action) {
      case 'navigate':
        handleNavigation(payload.nodeId);
        break;
      case 'view':
        handleGetNode(payload.nodeId);
        break;
      default:
        console.error('Acción no válida');
    }
  };

  return (
    <SafeAreaView style={styles.flex1}>
      <ErrorBoundary fallBackComponent={<ErrorNode />}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.container}>
          {nodesState.type === typeStateEnum.PENDING && <Loading />}
          {nodesState.type === typeStateEnum.FAIL && <ErrorNode />}
          {nodesState.type === typeStateEnum.DONE && (
            <ListNode nodes={nodesState.data} onPress={onActions} />
          )}
        </View>

        <ModalNote
          node={nodeState}
          onPress={handleGetFetchNode}
          visible={visibleModal}
          onClose={handleCloseModal}
        />
      </ErrorBoundary>
    </SafeAreaView>
  );
};

export default ParentsNodeScreen;
