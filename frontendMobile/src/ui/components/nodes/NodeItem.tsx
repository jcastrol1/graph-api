import React, { useCallback } from 'react';
import { Pressable, Text, View } from 'react-native';
import ColoredButton from '../common/ColoredButton';
import { Node } from '../../../domain/entities/Node';
import { styles } from './nodestyles';
import { ActionHandler } from '../../screens/ChildrenNodeScreen';

type Props = {
  onPress: ActionHandler;
  node: Node;
};

export const NodeItem = ({ onPress, node }: Props) => {
  const handleOnPress = useCallback(() => {
    onPress({ action: 'navigate', payload: { nodeId: node.id } });
  }, [onPress, node]);

  const handleDetail = useCallback(() => {
    onPress({ action: 'view', payload: { nodeId: node.id, locale: '' } });
  }, [onPress, node]);

  return (
    <Pressable
      style={({ pressed }) => [
        {
          backgroundColor: pressed ? 'rgb(210, 230, 255)' : 'white',
        },
      ]}
      onPress={handleOnPress}
    >
      <View style={styles.containerItem}>
        <View style={styles.sectionTitle}>
          <Text style={styles.title}> {node.title}</Text>
        </View>
        <ColoredButton color="#17e6fd" title="Details" onPress={handleDetail} />
      </View>
    </Pressable>
  );
};
