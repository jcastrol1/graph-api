import React, { FC } from 'react';
import { Text, View } from 'react-native';
import { styles } from './nodestyles';

export const ErrorNode: FC = () => {
  return (
    <View style={styles.centerContainer}>
      <Text style={styles.text}>{"Can't load nodes"}</Text>
    </View>
  );
};
