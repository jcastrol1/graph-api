import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  centerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  containerList: {
    width: '100%',
  },
  text: { fontSize: 20 },
  emptyList: {
    minHeight: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerItem: {
    height: 70,
    marginVertical: 7,
    marginHorizontal: 20,
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  sectionTitle: {
    width: '70%',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  containerSection: {
    height: 70,
    width: '100%',
    borderColor: '#d1d1d1',
    borderWidth: 1,
    paddingHorizontal: 30,
  },
  modalCard: {
    minHeight: 200,
    width: '100%',
  },
});
