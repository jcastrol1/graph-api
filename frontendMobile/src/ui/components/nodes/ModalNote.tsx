import React, { useEffect, useState } from 'react';
import CustomModal from '../../containers/CustomModal';
import { Text, View } from 'react-native';
import { Node } from '../../../domain/entities/Node';
import { State, typeStateEnum } from '../../../utils/hooks/useFetch';
import Loading from '../common/Loading';
import ColoredButton from '../common/ColoredButton';
import { styles } from './nodestyles';
import { SelectList } from 'react-native-dropdown-select-list';

type Props = {
  visible: boolean;
  onClose: () => void;
  node: State<Node>;
  onPress: ({ id, locale }: { id: number; locale?: string }) => void;
};
interface Locale {
  locale: string;
  label: string;
}
export const ModalNote = ({ visible, onClose, node, onPress }: Props) => {
  const [selected, setSelected] = React.useState<string>('');
  const [location, setlocation] = useState<Locale[]>([]);
  useEffect(() => {
    fetch('https://api-graph.tests.grupoapok.com/api/locales')
      .then((resp) => resp.json())
      .then((response) => {
        setlocation(response);
      });
  }, []);
  const handleTranslate = () => {
    if (node.type === typeStateEnum.DONE) {
      onPress({ id: node.data.id, locale: selected });
    }
  };
  return (
    <CustomModal visible={visible} onClose={onClose}>
      {node.type === typeStateEnum.DONE && (
        <View style={styles.modalCard}>
          <SelectList
            setSelected={setSelected}
            data={location.map((l) => ({ key: l.locale, value: l.label }))}
            save="key"
          />
          <ColoredButton
            onPress={handleTranslate}
            title="Translate"
            color="#30f85f"
          />
          <View style={styles.container}>
            <Text style={styles.title}>{node.data.title}</Text>
            {node.data.parent && (
              <Text style={styles.text}>{`Parent:${node.data.parent}`}</Text>
            )}
            {node.data?.translation && (
              <Text
                style={styles.text}
              >{`${node.data.translation[0].title}`}</Text>
            )}
          </View>
        </View>
      )}
      {node.type === typeStateEnum.PENDING && <Loading />}
    </CustomModal>
  );
};
