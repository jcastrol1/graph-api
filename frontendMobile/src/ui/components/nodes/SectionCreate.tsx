import React from 'react';
import { View } from 'react-native';
import ColoredButton from '../common/ColoredButton';
import { styles } from './nodestyles';

type Props = {
  id: number | null;
  onPress: () => void;
};

export const SectionCreate = ({ id, onPress }: Props) => {
  return (
    <View style={styles.containerSection}>
      <ColoredButton
        onPress={onPress}
        title={id === null ? 'Create parent node' : `Create child of ${id} `}
        color="#30f85f"
      />
    </View>
  );
};
