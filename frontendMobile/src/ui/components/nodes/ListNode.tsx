import React, { useMemo } from 'react';
import { FlatList, Text, View } from 'react-native';
import { NodeItem } from './NodeItem';
import { Node } from '../../../domain/entities/Node';
import { styles } from './nodestyles';
import { ActionHandler } from '../../screens/ChildrenNodeScreen';

type Props = {
  nodes: Node[];
  onPress: ActionHandler;
};

export const ListNode = ({ nodes, onPress }: Props) => {
  const emptyComponent = useMemo(
    () => (
      <View style={styles.emptyList}>
        <Text style={styles.text}>There are no child nodes</Text>
      </View>
    ),
    [],
  );
  return (
    <FlatList
      style={styles.containerList}
      data={nodes}
      keyExtractor={(item, index) => `${item.id}-${index}`}
      initialNumToRender={10}
      windowSize={5}
      renderItem={({ item }) => <NodeItem onPress={onPress} node={item} />}
      ListEmptyComponent={emptyComponent}
    />
  );
};
