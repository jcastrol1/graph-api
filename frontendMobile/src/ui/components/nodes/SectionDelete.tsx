import React from 'react';
import { View } from 'react-native';
import ColoredButton from '../common/ColoredButton';
import { styles } from './nodestyles';

type Props = {
  onPress: () => void;
};

export const SectionDelete = ({ onPress }: Props) => {
  return (
    <View style={styles.containerSection}>
      <ColoredButton onPress={onPress} title={'Delete'} color="#f83030" />
    </View>
  );
};
