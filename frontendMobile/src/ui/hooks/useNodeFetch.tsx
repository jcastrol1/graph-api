import { Node } from '../../domain/entities/Node';
import { NodesService } from '../../services/NodesService';
import { useFetch } from '../../utils/hooks/useFetch';

export const useNodeFetch = () => {
  const [nodeState, fetchNode] = useFetch<Node>();

  const handleCreateFetchNode = (parent: number, locales: string[]) => {
    if (parent) {
      fetchNode((signal) => NodesService.saveNode(signal, { parent, locales }));
    }
  };
  const handleDeleteFetchNode = (id: number) => {
    if (id) {
      fetchNode((signal) => NodesService.deleteNode(signal, id));
    }
  };
  const handleGetFetchNode = ({
    id,
    locale,
  }: {
    id: number;
    locale?: string;
  }) => {
    console.log('fuer<', id, locale);
    if (id && !locale) {
      console.log('aqui');
      fetchNode((signal) => NodesService.getNode(signal, id));
    }
    if (id && locale) {
      console.log('aca');
      fetchNode((signal) => NodesService.getNodewithLocale(signal, id, locale));
    }
    console.log('fuer<', id, locale);
  };

  return [
    nodeState,
    handleGetFetchNode,
    handleCreateFetchNode,
    handleDeleteFetchNode,
  ] as const;
};
