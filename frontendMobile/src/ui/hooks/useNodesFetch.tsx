import { useEffect } from 'react';
import { Node } from '../../domain/entities/Node';
import { NodesService } from '../../services/NodesService';
import { useFetch } from '../../utils/hooks/useFetch';

export const useNodesFetch = (id: number | null) => {
  const [nodesState, fetchNodes] = useFetch<Node[]>();

  const handleFetchNodes = () => {
    if (id === null) {
      fetchNodes(NodesService.getParentNodes);
    }
    if (typeof id === 'number') {
      fetchNodes((signal) => NodesService.getChildNodes(id, signal));
    }
  };

  useEffect(() => {
    handleFetchNodes();
  }, []);

  return [nodesState, handleFetchNodes] as const;
};
