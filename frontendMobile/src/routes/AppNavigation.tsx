import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { TransitionSpec } from '@react-navigation/stack/src/types';
import ParentsNodeScreen from '../ui/screens/ParentsNodeScreen';
import ChildrenNodeScreen from '../ui/screens/ChildrenNodeScreen';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createStackNavigator();

export function AppNavigation() {
  const config: TransitionSpec = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerTitleAlign: 'center', // Centrar el título del encabezado
          transitionSpec: {
            open: config,
            close: config,
          },
        }}
      >
        <Stack.Screen
          name="Home"
          component={ParentsNodeScreen}
          initialParams={{ id: null }}
        />
        <Stack.Screen name="Nodes" component={ChildrenNodeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
